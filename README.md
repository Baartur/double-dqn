# DOUBLE-DQN

An implementation of the Double DQN (Hasselt, 2015) for the project of Reinforcement Learning at ENSAE Paris.

## Project instruction

Instructions can be found [here](https://sites.google.com/view/rlensae2021/evaluation) at time of writting. 

## Project report

Our work will be presented in a PDF report available before May 11th.
The application part is detailed in the main notebook <code>Double_DQN_ATTIA_SAIDI_ARTUR.ipynb</code>
