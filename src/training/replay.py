import torch

# Common impots
import numpy as np
import scipy
import math
import json
import time
import operator

class ExperienceReplay(object): 
    def __init__(self, max_memory=100, discount=.75):  
        self.max_memory = max_memory
        self.memory     = []
        self.discount   = discount

    def remember(self, states, game_over):
        self.memory.append([states, game_over])

        #Delete the first experience if the memory is too long
        if len(self.memory) > self.max_memory:
            del self.memory[0]

    def get_batch(self, device, model, target_model, batch_size=10):
        model.eval()
        len_memory = len(self.memory)
        
        #number of actions in action space
        num_actions = model.output.out_features
        
        #states is an experience : [input_t_minus_1, action, reward, input_t],
        #so memory[0] is state and memory[0][0][0].shape[1] is the size of the input
        env_dim = self.memory[0][0][0].shape[1]
        
        # if batch_size<len_memory (it is mostly the case), 
        # then input is a matrix with batch_size rows and size of obs columns
        
        inputs = torch.zeros((min(len_memory, batch_size), env_dim)).to(device)
        #targets is a matrix with batch_size rows and number of actions columns
        targets = torch.zeros((inputs.shape[0], num_actions)).to(device)
        
        for i, idx in enumerate(np.random.choice(range(len_memory), inputs.shape[0], replace=False)):        
            #get experience number idx, idx being a random number in [0,length of memory]
            #There are batch_size experiences that are drawn
            state, action, reward, next_state = self.memory[idx][0]

            #next_state = torch.Tensor(next_state).to(device)

            #Is the game over ? AKA done in gym
            game_over = self.memory[idx][1]

            #The inputs of the NN are the state of the experience drawn
            inputs[i] = state
            targets[i] = target_model(state)
            
            #Q_sa=Q_target(s,argmax_a'{Q(s',a')}
            index, maxima = max(enumerate(model(next_state)[0]), key=operator.itemgetter(1))
            Q_sa = target_model(next_state)[0][index]
              
            # if game_over is True then the sequence is terminated 
            if game_over:  
                targets[i, action] = reward
            else:
                # the target for this particular experience is : reward_t + gamma * max_a' Q(s', a')
                targets[i, action] = reward + self.discount * Q_sa

        return inputs, targets