# Common impots
import numpy as np
import scipy
import math
import json
import time
import operator

# DL imports
import torch
from torch import nn
from torch.nn import functional as F
from torch.optim import Adam
from torch.autograd import Variable

from .replay import ExperienceReplay

def train(device, model, target_model, optimizer, env, epsilon, 
          decay_rate, max_memory, batch_size, target_score=200, double=True, max_episodes=25):
    time_step    = 0
    win_cnt      = 0
    actual_total = 0
    episodes     = 0
    everyC       = 3
    
    observation_shape = env.observation_space.shape[0]
    num_actions = env.action_space.n 
    exp_replay = ExperienceReplay(max_memory=max_memory)
    MSE = nn.MSELoss()

    eps_threshold = 0
    histo = {"reward":[], "time_steps":[]}

    while (episodes<max_episodes):#(time_step < max_time_steps) and (episodes<max_episodes):
        # reset variables
        acc_reward = 0
        C          = 0
        episodes  += 1
        
        # env.reset() : reset the environment, get first observation
        input_t = env.reset()
        input_t = torch.Tensor(input_t.reshape((1, observation_shape))).to(device)

        # Start the game
        game_over = False    
        while not game_over:        
            # set this state to be the last state
            input_tm1 = input_t
            
            # Explore a lot in the beggining and exploit at the end
            eps_threshold = 0.05 + (epsilon - 0.05) * math.exp(-1. * time_step / decay_rate)

            if np.random.rand() <= eps_threshold:
                # Explore with probability eps_threshold
                action = np.random.randint(0, num_actions, size=1)[0]
            else:
                #exploitation
                if double:
                    q = model(input_tm1)
                else:
                    q = target_model(input_tm1)
                action = torch.argmax(q[0]).item()

            # Apply action        
            input_t, reward, game_over, _ = env.step(action)
            input_t = torch.Tensor(input_t).view(1, observation_shape).to(device)
            
            acc_reward += reward

            # Store experience
            exp_replay.remember([input_tm1, action, reward, input_t], game_over)
            
            # Update periodically target network simply by copying model every C iterations
            if C % everyC == 0:
                target_model.load_state_dict(model.state_dict())
            C += 1
            
            # get batch we will train on
            inputs, targets = exp_replay.get_batch(device, model, target_model, batch_size=batch_size)

            # start of actual training time
            t2 = time.time()

            # train 
            model.train()
            optimizer.zero_grad()
            output = model(inputs)
            mse = MSE(output, targets)
            mse.backward()
            optimizer.step()

            # end of actual training time
            t3 = time.time()
            actual_total += t3-t2

            # increment time-step
            time_step += 1
            
            # end game if max score is reached
            if acc_reward >= target_score:
                game_over = True
                
        if acc_reward >= 200:
            win_cnt += 1

        print(time_step,'time steps done, ', episodes,'episodes done. Reward :', acc_reward,'epsilon thresh : ', eps_threshold)
        histo["reward"].append(acc_reward)
        histo["time_steps"].append(time_step)
    return time_step, episodes, actual_total, win_cnt, histo