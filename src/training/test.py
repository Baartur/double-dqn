import torch
from torch import nn
from torch.nn import functional as F
from torch.optim import Adam
from torch.autograd import Variable

def test(device, model, env, test_episodes, max_memory, target_score):
    model.eval()
    win_cnt = 0
    observation_shape = env.observation_space.shape[0]
    num_actions = env.action_space.n 
    
    total_reward = 0
    
    for episode in range(test_episodes):
        #set accumulated reward to 0
        episode_reward = 0
        
        #env.reset() : reset the environment, get first observation
        input_t = env.reset()
        input_t = input_t.reshape((1,observation_shape))
        
        #the game starts, so set game_over to False
        game_over = False
        

        while not game_over:
            input_tm1 = torch.Tensor(input_t).to(device)
            
            # Get action
            q = model(input_tm1)
            action = torch.argmax(q[0]).cpu().numpy()

            #apply action, get rewards and new state
            input_t, reward, game_over, _ = env.step(action)
            input_t = torch.Tensor(input_t).view(1,observation_shape)
            
            #Accumulate reward
            episode_reward += reward

            #end game if max score is reached
            if episode_reward >= target_score:
                game_over=True

        #Total reward over the episodes
        total_reward+=episode_reward

        print(episode+1,'episodes done. Reward :', episode_reward)

    print('The average reward over the test was :', total_reward / test_episodes)