import os
from pathlib import Path
from os.path import abspath, join

# Careful: code is calibrated for a file paths.py located in directory "settings"

dir_path = os.path.dirname(__file__)

#### Source code paths
SRC_DIR = Path(abspath(join(dir_path, "..")))
SETTINGS_DIR = Path(abspath(join(SRC_DIR, "settings")))
MODELS_DIR = Path(abspath(join(SRC_DIR, "models")))
TRAINING_DIR = Path(abspath(join(SRC_DIR, "training")))

#### Data directories
DATA_DIR = Path(abspath(join(dir_path, "..", "..","data")))
GRAPHS_DIR = Path(abspath(join(DATA_DIR,"graphs")))