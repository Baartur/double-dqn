from torch import nn
from torch.nn import functional as F


class DqnMlp(nn.Module):
    def __init__(self, in_dim, out_dim, hidden_dim):
        super(DqnMlp, self).__init__()
        self.intput = nn.Linear(in_dim, hidden_dim)
        self.fc1    = nn.Linear(hidden_dim, hidden_dim)
        self.fc2    = nn.Linear(hidden_dim, hidden_dim)
        self.output = nn.Linear(hidden_dim, out_dim)

    def forward(self, x):
        x = F.relu(self.intput(x))
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.output(x)
        return x


class DqnCnn(nn.Module):
    def __init__(self, in_dim, out_dim):
        super(DqnCnn, self).__init__()
        self.in_dim = in_dim
        self.out_dim = out_dim
        self.cnn = nn.Sequential(
            nn.Conv2d(self.in_dim, 32, kernel_size=8, stride=4),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.Conv2d(32, 64, kernel_size=4, stride=2),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.Conv2d(64, 64, kernel_size=3, stride=1),
            nn.BatchNorm2d(64),
            nn.ReLU())
        self.classifier = nn.Sequential(
            nn.Linear(64 * 7 * 7, 512),
            nn.ReLU(),
            nn.Linear(512, self.out_dim))

    def forward(self, x):
        # assert(list(x.size()[-3]) == self.in_dim)
        # debug_here()
        x = self.cnn(x)
        x = x.view(x.size(0), -1)
        x = self.classifier(x)